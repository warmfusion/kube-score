#!/bin/sh
#
# A convinence wrapper around the kube-score script
# to make its execution in CI a little easier for me.
#

for X in $@;
do
  echo "-----"
  echo "CHECKING ${X}...."
  if [ "find ${X} -type f -name '*.yaml'" ] ;
  then
    kube-score $(find ${X} -type f -name "*.yaml")
    if [ $? -gt 0 ] ; then
      ERRORS=1
    fi
  else
    ERRORS=1
    echo "ERROR: No yaml found in [${X}]"
  fi

done


echo "-----"
if [ $ERRORS ]; then
  echo "There have been errors - Fail"
  echo ""
  echo "(╯°□°）╯︵ ┻━┻"
  exit $ERRORS
else
  echo "... No Warnings or Criticals found ..."
  echo ""
  echo "     ,.- Thanks for being great"
  echo "ʕ•ᴥ•ʔ"
fi
