FROM alpine

# Install kube-score
RUN wget https://github.com/zegl/kube-score/releases/download/v0.7.0/kube-score_0.7.0_linux_amd64.tar.gz -qO - | \
   tar -zx -C /usr/bin/ && chmod +x /usr/bin/kube-score
   
RUN wget https://github.com/kubernetes-sigs/kustomize/releases/download/v2.0.1/kustomize_2.0.1_linux_amd64 -qO /usr/bin/kustomize && \
    chmod +x /usr/bin/kustomize

COPY kube-score-wrapper.sh /usr/bin/kube-score-wrapper
